import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@NoArgsConstructor
public class Cliente {

    private String nome;
    private String email;
    private String telefone;
    private String cpf;

}
