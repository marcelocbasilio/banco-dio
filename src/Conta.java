import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Getter
@NoArgsConstructor
public abstract class Conta implements IConta {

    private static final int AGENCIA_PADRAO = 1;
    private static int SEQUENCIAL = 1;

    protected int agencia;
    protected int numero;
    protected double saldo;
    private Cliente cliente;

    public Conta(Cliente cliente) {
        this.agencia = AGENCIA_PADRAO;
        this.numero = SEQUENCIAL++;
        this.cliente = cliente;
    }

    @Override
    public double sacar(double valor) {
        if (this.saldo > 0.0) {
            if (this.saldo >= valor) {
                this.saldo -= valor;
                return valor;
            }
        }

        return 0.0;
    }

    @Override
    public void depositar(double valor) {
        this.saldo += valor;
    }

    @Override
    public void transferir(double valor, @NotNull IConta contaDestino) {
//        this.sacar(valor);
        contaDestino.depositar(this.sacar(valor));
    }

    protected void imprimirInfosComuns() {
        System.out.printf("> Titular: %s%n", this.cliente.getNome());
        System.out.printf("> Agência: %d%n", this.agencia);
        System.out.printf("> Número: %d%n", this.numero);
        System.out.printf("> Saldo: %.2f%n", this.saldo);
    }

}
