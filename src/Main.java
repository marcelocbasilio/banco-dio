public class Main {

    public static void main(String[] args) {

        Cliente cliente = new Cliente();
        cliente.setNome("Roberto Miranda Andrade");
        cliente.setEmail("roberto@gmail.com");
        cliente.setTelefone("+55 85 9 8888-7777");
        cliente.setCpf("111.111.111-11");

        Conta cc = new ContaCorrente(cliente);
        Conta cp = new ContaPoupanca(cliente);

        cc.depositar(100.0);
        cc.transferir(50.0, cp);

        cc.imprimirExtrato();
        cp.imprimirExtrato();
    }
}